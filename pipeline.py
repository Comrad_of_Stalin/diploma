from __future__ import unicode_literals, print_function, division
from io import open
import unicodedata
import string
import re
import random

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F


from seq2seq.all import load_dump, evaluate
from audio_reader.transcriber import pipeline
from audio_reader.reader import record_to_file

import pyttsx3
from gtts import gTTS

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

engine = pyttsx3.init()
engine.setProperty('rate', 150)


def load_seq2seq_models(app, hidden_size):
    app.config['SEQ2SEQ'] = {}
    app.config['SEQ2SEQ']['HID_SIZE'] = hidden_size
    encoder, decoder, inp_lang, out_lang = load_dump(app.config['SEQ2SEQ_DUMP'], device_type='cpu', hidden_size=app.config['SEQ2SEQ']['HID_SIZE'])
    conf = app.config['SEQ2SEQ']
    conf['ENCODER'] = encoder
    conf['DECODER'] = decoder
    conf['INP_LANG'] = inp_lang
    conf['OUT_LANG'] = out_lang


def text_pipeline(encoder, decoder, inp_lang, out_lang, text):
    ans, _ = evaluate(encoder, decoder, inp_lang, out_lang, text)
    return ' '.join(ans[:-1])


def full_pipeline(encoder, decoder, inp_lang, out_lang, pyttsx3_engine=engine, src_wav_path=None, dst_wav_path=None):
    transcriptions = pipeline(src_wav_path)
    text = transcriptions.iloc[0]['Транскрибация']
    print(text)
    ans, _ = evaluate(encoder, decoder, inp_lang, out_lang, text)
    print(ans)
    sent = ' '.join(ans[:-1])
    tts = gTTS(text=sent, lang='en')
    tts.save(dst_wav_path)
    print('Saved into', dst_wav_path)


def parse_request(input_str, format_str):
    if format_str == 'wav2wav':
        return
    if format_str == 'wav2text':
        return
    if format_str == 'text2wav':
        return
    pass
