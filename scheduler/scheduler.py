class Scheduler:

    def __init__(self, n_workspaces, n_therads_per_ws=16, request_spec=None):
        if n_workspaces or n_therads_per_ws or request_spec:
            pass
        pass

    def run(self):
        pass

    def schedule(self, request):
        pass

    def stop(self):
        pass

    def get_stat(self):
        pass

    def preprocess_data(self, request_data):
        pass

    def validate_users_data(self, user):
        pass


class Workspace:
    def __init__(self, root_dir, log_sir, n_threads):
        pass

    def initialize(self):
        pass

    def _load(self, dir):
        pass

    def write_log(self, event):
        pass

    def init_db_connection(self, client, host, port):
        pass


class Worker:
    def __init__(self, model_type, path, spec):
        pass

    def process_request(self, request):
        pass

    def write_log_to_db(self, request, client, host, port):
        pass

    def stop(self):
        pass