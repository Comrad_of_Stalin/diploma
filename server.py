from utils import generate_random_start, generate_from_seed
from flask import Flask, render_template, request, jsonify, Response, redirect, send_file
from wtforms import Form, TextField, validators, SubmitField, DecimalField, IntegerField, FileField
from werkzeug.utils import secure_filename

from pipeline import full_pipeline, load_seq2seq_models

import os
from uuid import uuid4

# Create app
app = Flask(__name__)
app.config['REQUEST_DIR'] = '/home/ubuntu/pipeline/web/requests'
app.config['SEQ2SEQ_DUMP'] = '/home/ubuntu/pipeline/nmt/dump4'

if not os.path.exists(app.config['REQUEST_DIR']):
    os.makedirs(app.config['REQUEST_DIR'])

load_seq2seq_models(app, hidden_size=512)


class ReusableForm(Form):
    """User entry form for entering specifics for generation"""
    # text
    text = TextField("Enter some text for translation:", validators=[validators.InputRequired()])
    # wav files
    wav_files = FileField('Enter wav files here:', validators=[validators.InputRequired(),
                                                 validators.DataRequired()])
    # Submit button
    submit = SubmitField("Enter")



def handle_request(request, attr_name):
    file = request.files[attr_name]

    req_id = str(uuid4())
    req_dir = os.path.join(app.config['REQUEST_DIR'], req_id)

    os.mkdir(req_dir)

    src_wav_path = os.path.join(req_dir, 'src.wav')
    dst_wav_path = os.path.join(req_dir, 'dst.wav')

    file.save(src_wav_path)

    full_pipeline(
        app.config['SEQ2SEQ']['ENCODER'],
        app.config['SEQ2SEQ']['DECODER'],
        app.config['SEQ2SEQ']['INP_LANG'],
        app.config['SEQ2SEQ']['OUT_LANG'],
        src_wav_path=src_wav_path,
        dst_wav_path=dst_wav_path
    )
    return req_id


# Home page
@app.route("/", methods=['GET', 'POST'])
def home():
    """Home page of app with form"""
    # Create form
    form = ReusableForm(request.form)

    # On form entry and all conditions met
    print('REQ M', request.method)
    if request.method == 'POST':  # and form.validate():
        req_id = handle_request(request, attr_name='file')
        return render_template('response.html', link='/getfile/' + req_id) # redirect('/getfile/' + req_id)

    return render_template('index.html', form=form)

# Home page
@app.route("/main", methods=['GET', 'POST'])
def main():
    print('!', request.files)

    # On form entry and all conditions met
    print('REQ M', request.method)
    if request.method == 'POST':  # and form.validate():
        req_id = handle_request(request, attr_name='wav-attachment-file')
        return render_template('response.html', link='/getfile/' + req_id)  # redirect('/getfile/' + req_id)

    return render_template('main.html')


@app.route('/getfile/<name>')
def get_output_file(name):
    file_name = os.path.join(app.config['REQUEST_DIR'], name, 'dst.wav')
    print('FILE NAME', file_name)
    if not os.path.isfile(file_name):
        return jsonify({"message": "still processing"})
    return send_file(file_name)


if __name__ == "__main__":
    print(("* Loading Keras model and Flask starting server..."
           "please wait until server has fully started"))
    # Run app
    app.run(host="0.0.0.0", port=8080)
