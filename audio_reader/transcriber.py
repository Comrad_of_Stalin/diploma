#!/usr/bin/python
import os
import shutil
import time
import argparse
import glob
import logging
import csv
from pathlib import Path
from tqdm import tqdm
from multiprocessing import Pool, cpu_count
from .tools import data_preparator, segmenter, recognizer, transcriptions_parser
from .tools.utils import make_ass, delete_folder, make_wav_scp, create_logger, prepare_wav


from pathlib import Path
import subprocess
import pandas as pd
from kaldi.segmentation import NnetSAD, SegmentationProcessor
from kaldi.asr import NnetLatticeFasterRecognizer
from kaldi.decoder import LatticeFasterDecoderOptions
from kaldi.nnet3 import NnetSimpleComputationOptions
from kaldi.util.table import SequentialMatrixReader, CompactLatticeWriter


def start_pipeline(data, pkg):
    """
    Запуск пайплайна распознавания речи

    Аргументы:
        wav: путь к .WAV файлу аудио
    """
    wav = data['WAV']

    wav = prepare_wav(wav)
    wav_name = Path(wav).name
    wav_stem = Path(wav).stem
    temp = str(Path(data['TEMP_DIR']) / wav_stem)
    os.makedirs(temp, exist_ok=True)
    wav_scp = str(Path(temp) / 'wav.scp')
    make_wav_scp(wav, wav_scp)

    def terminate_pipeline(is_error, message):
        if is_error:
            data['LOGGER'].error(message)
            os.rename(wav, str(data['ERROR_DIR'] / wav_name))
        try:
            delete_folder(temp)
        except:
            data['LOGGER'].error("Не удалось удалить временные файлы для '{}'".format(wav_name))

    try:
        data['LOGGER'].info("Запуск сегментации файла '{}'".format(wav_name))
        segm = segmenter.Segmenter(wav_scp, pkg['SEGM_MODEL'], pkg['SEGM_POST'], pkg['SEGM_CONF'], temp)
        segments = segm.segment()
        data['LOGGER'].info("Завершение сегментации файла '{}'".format(wav_name))
    except:
        terminate_pipeline(True, "Не удалось выполнить сегментацию файла '{}'".format(wav_name))
        return
    if os.stat(segments).st_size == 0:
        terminate_pipeline(True, "В файле '{}' отсутствуют сегменты".format(wav_name))
        return

    try:
        data['LOGGER'].info("Запуск извлечения сегментов из файла '{}'".format(wav_name))
        wav_segments_scp, utt2spk, spk2utt = segm.extract_segments(segments)
        data['LOGGER'].info("Завершение извлечения сегментов из файла '{}'".format(wav_name))
    except:
        terminate_pipeline(True, "Не удалось извлечь сегменты из файла '{}'".format(wav_name))
        return
    try:
        data['LOGGER'].info("Запуск распознавания файла '{}'".format(wav_name))
        rec = recognizer.Recognizer(wav_segments_scp,
                                    pkg['REC_MODEL'],
                                    pkg['REC_GRAPH'],
                                    pkg['REC_WORDS'],
                                    pkg['REC_CONF'],
                                    pkg['REC_ICONF'],
                                    spk2utt,
                                    temp)
        transcriptions = rec.recognize(wav_stem)
        data['LOGGER'].info("Завершение распознавания файла '{}'".format(wav_name))
    except:
        terminate_pipeline(True, "Не удалось выполнить распознавание файла '{}'".format(wav_name))
        return
    try:
        data['LOGGER'].info("Запуск формирования субтитров для файла '{}'".format(wav_name))
        ass = str(data['OUTPUT_DIR'] / str('ass/' + wav_stem + '.ass'))
        make_ass(wav_name, segments, transcriptions, utt2spk, ass)
        data['LOGGER'].info("Завершение формирования субтитров для файла '{}'".format(wav_name))
    except:
        terminate_pipeline(True, "Не удалось сформировать субтитры для файла '{}'".format(wav_name))
        return
    try:
        data['LOGGER'].info("Запуск парсинга транскрибации для файла '{}'".format(wav_name))
        pars = transcriptions_parser.TranscriptionsParser(str(data['OUTPUT_DIR'] / 'ass'), data['OUTPUT_DIR'], data['LOGGER'] if IS_LOG else '',
                                                          1, 1, data['CSV'])
        pars.process_batch_files([ass])
        data['LOGGER'].info("Завершение парсинга транскрибации для файла '{}'".format(wav_name))
    except:
        terminate_pipeline(True, "Не удалось распарсить транскрибацию файла '{}'".format(wav_name))
        return

    if data['IS_DELETE_WAV'] or data['SLEEP_TIME']:
        data['LOGGER'].info("Запуск удаления файла '{}'".format(wav_name))
        try:
            os.remove(wav)
        except:
            data['LOGGER'].error("Не удалось удалить файл '{}'".format(wav_name))
        data['LOGGER'].info("Завершение удаления файла '{}'".format(wav_name))

    terminate_pipeline(False, None)


def load_dump(model_dir):
    pkg = {}
    pkg['REC_MODEL'] = os.path.join(model_dir, 'final.mdl')
    pkg['REC_GRAPH'] = os.path.join(model_dir, 'HCLG.fst')
    pkg['REC_WORDS'] = os.path.join(model_dir, 'words.txt')
    pkg['REC_CONF'] = os.path.join(model_dir, 'conf/mfcc.conf')
    pkg['REC_ICONF'] = os.path.join(model_dir, 'conf/ivector_extractor.conf')
    pkg['SEGM_MODEL'] = os.path.join(model_dir, 'final.raw')
    pkg['SEGM_CONF'] = os.path.join(model_dir, 'conf/mfcc_hires.conf')
    pkg['SEGM_POST'] = os.path.join(model_dir, 'conf/post_output.vec')

    pkg['PROCESSES'] = cpu_count()
    pkg['IS_LOG'] = True
    pkg['IS_DELETE_WAV'] = False
    pkg['SLEEP_TIME'] = None
    pkg['DELTA_TIME'] = None

    return pkg


def prepare_data(pkg, wav, output_dir):
    prep = data_preparator.DataPreparator(wav, str(output_dir), pkg['IS_LOG'])
    data = {}
    data['WAV'] = wav
    data['LOG_DIR'], data['TEMP_DIR'], data['ASS_DIR'], data['ERROR_DIR'] = prep.create_directories()
    log_name = str(data['LOG_DIR'] / str(time.strftime('%Y%m%d-%H%M%S') + '.log'))
    data['LOGGER'] = create_logger('logger', 'file', logging.DEBUG, log_name)
    data['CSV'] = os.path.join(str(output_dir), str('transcriptions_' + time.strftime('%Y%m%d-%H%M%S') + '.csv'))
    return data


def make_spk2utt(utt2spk, S2U_PATH):
    """
    Формирование spk2utt файла

    Аргументы:
        utt2spk: путь к файлу сопоставления сегментов и говорящих

    Результат:
        spk2utt: путь к файлу перечисления сегментов для каждого говорящего
    """
    spk2utt = str(S2U_PATH)
    utt2spk_df = pd.read_csv(utt2spk, sep='\t', header=None, names=['utt_id', 'speaker_id'])
    spk2utt_df = utt2spk_df.groupby('speaker_id')['utt_id'].apply(lambda x: ' '.join(x)).reset_index()
    spk2utt_df.to_csv(spk2utt, sep='\t', index=False, header=False)
    return spk2utt


def make_segmentation(MODEL_DIR, SCP_PATH, SEGMS_PATH, SCP_SEGMS_PATH, S2U_PATH, U2S_PATH, TEMP_DIR):
    decodable_opts = NnetSimpleComputationOptions()
    decodable_opts.extra_left_context = 79
    decodable_opts.extra_right_context = 21
    decodable_opts.extra_left_context_initial = 0
    decodable_opts.extra_right_context_final = 0
    decodable_opts.frames_per_chunk = 150
    decodable_opts.acoustic_scale = 0.3

    model = NnetSAD.read_model(str(MODEL_DIR / 'final.raw'))
    post = NnetSAD.read_average_posteriors(str(MODEL_DIR / 'conf/post_output.vec'))
    transform = NnetSAD.make_sad_transform(post)
    graph = NnetSAD.make_sad_graph()

    sad = NnetSAD(model, transform, graph, decodable_opts=decodable_opts)
    seg = SegmentationProcessor([2])

    feats_rspec = "ark:compute-mfcc-feats --verbose=0 --config=" + str(
        MODEL_DIR / 'conf/mfcc_hires.conf') + " scp:" + SCP_PATH + " ark:- |"
    print(feats_rspec)
    with SequentialMatrixReader(feats_rspec) as f, open(SEGMS_PATH, 'w') as s:
        for i, (key, feats) in enumerate(f):
            print(i)
            out = sad.segment(feats)
            segs, _ = seg.process(out['alignment'])
            seg.write(key, segs, s)

    with open(SEGMS_PATH, 'r') as s, \
            open(SCP_SEGMS_PATH, 'w') as ws, \
            open(U2S_PATH, 'w') as u:
        for segment in s:
            segment_info = segment.split(' ')
            segment_id = segment_info[0]
            speaker_id = segment.split(' ')[1].split('.')[-1] or segment_id
            ws.write(segment_id + '\t{}/'.format(TEMP_DIR) + segment_id + '.wav' + '\n')
            u.write(segment_id + '\tКанал ' + speaker_id + '\n')
    make_spk2utt(U2S_PATH, S2U_PATH)
    extract_command = "extract-segments scp:" + SCP_PATH + " " + SEGMS_PATH + " scp:" + SCP_SEGMS_PATH
    with subprocess.Popen(extract_command, shell=True):
        pass
    print('DONE make_segmentation')


def recognize(MODEL_DIR, SCP_PATH, S2U_PATH, TRANS_PATH):
    decoder_opts = LatticeFasterDecoderOptions()
    decoder_opts.beam = 13
    decoder_opts.max_active = 7000
    decodable_opts = NnetSimpleComputationOptions()
    decodable_opts.acoustic_scale = 1.0
    decodable_opts.frame_subsampling_factor = 3

    model = str(MODEL_DIR / 'final.mdl')
    graph = str(MODEL_DIR / 'HCLG.fst')
    words = str(MODEL_DIR / 'words.txt')

    asr = NnetLatticeFasterRecognizer.from_files(model, graph, words,
                                                 decoder_opts=decoder_opts,
                                                 decodable_opts=decodable_opts)

    feats_rspec = ("ark:compute-mfcc-feats --config=" + str(
        MODEL_DIR / 'conf/mfcc.conf') + " scp:" + SCP_PATH + " ark:- |")
    ivectors_rspec = (feats_rspec + "ivector-extract-online2 "
                                    "--config=" + str(MODEL_DIR / 'conf/ivector_extractor.conf') + " "
                                                                                                   "ark:" + S2U_PATH + " ark:- ark:- |")
    lat_wspec = "ark:| gzip -c > lat.gz"
    print(feats_rspec)
    print()
    print(ivectors_rspec)
    with SequentialMatrixReader(feats_rspec) as feats_reader, \
            SequentialMatrixReader(ivectors_rspec) as ivectors_reader, \
            CompactLatticeWriter(lat_wspec) as lat_writer:
        for (fkey, feats), (ikey, ivectors) in zip(feats_reader, ivectors_reader):
            print('X')
            assert (fkey == ikey)
            out = asr.decode((feats, ivectors))
            lat_writer[fkey] = out['lattice']
            with open(TRANS_PATH, 'a') as f:
                f.write(fkey + '\t' + out['text'].lower() + '\n')

    transcriptions = pd.read_csv(TRANS_PATH, header=None, sep='\t', names=['Сегмент', 'Транскрибация'])
    return transcriptions


def pipeline(wav):
    MODEL_DIR=Path('/home/ubuntu/pipeline/asr/model')
    PREFIX = Path('/home/ubuntu/pipeline/examples/data')
    SCP_PATH=str(PREFIX/'wav.scp')
    SEGMS_PATH=str(PREFIX/'segments')
    SCP_SEGMS_PATH=str(PREFIX/'segemnts.scp')
    S2U_PATH=str(PREFIX/'spk2utt')
    U2S_PATH=str(PREFIX/'utt2spk')
    TEMP_DIR=str(PREFIX/'temp')
    TRANS_PATH=str(PREFIX/'transcriptions')

    if os.path.exists(PREFIX):
        shutil.rmtree(PREFIX)
        os.makedirs(PREFIX)
        os.makedirs(TEMP_DIR)

    wav = prepare_wav(wav)
    # wav_name = Path(wav).name
    wav_stem = Path(wav).stem
    print('WAV STEM', wav_stem)
    temp = str(Path(TEMP_DIR) / wav_stem)
    os.makedirs(temp, exist_ok=True)
    # wav_scp = str(Path(temp) / 'wav.scp')
    make_wav_scp(wav, SCP_PATH)
    print('WAV SCP', SCP_PATH)

    make_segmentation(
        MODEL_DIR=MODEL_DIR,
        SCP_PATH=SCP_PATH,
        SEGMS_PATH=SEGMS_PATH,
        SCP_SEGMS_PATH=SCP_SEGMS_PATH,
        U2S_PATH=U2S_PATH,
        S2U_PATH=S2U_PATH,
        TEMP_DIR=TEMP_DIR
    )

    transcriptions_df = recognize(
        MODEL_DIR=MODEL_DIR,
        SCP_PATH=SCP_SEGMS_PATH,
        S2U_PATH=S2U_PATH,
        TRANS_PATH=TRANS_PATH
    )

    return transcriptions_df
# 
# if __name__ == '__main__':
#     parser = argparse.ArgumentParser(description='Запуск процедуры распознавания речи')
#     parser.add_argument('wav', metavar='WAV', help='Путь к .WAV файлам аудио')
#     parser.add_argument('output', metavar='OUT', help='Путь к директории с результатами распознавания')
#     parser.add_argument('-rm', '--rec_model', help='Путь к .MDL файлу модели распознавания')
#     parser.add_argument('-rg', '--rec_graph', help='Путь к .FST файлу общего графа распознавания')
#     parser.add_argument('-rw', '--rec_words', help='Путь к .TXT файлу текстового корпуса')
#     parser.add_argument('-rc', '--rec_conf', help='Путь к .CONF конфигурационному файлу распознавания')
#     parser.add_argument('-ri', '--rec_iconf', help='Путь к .CONF конфигурационному файлу векторного экстрактора')
#     parser.add_argument('-sm', '--segm_model', help='Путь к .RAW файлу модели сегментации')
#     parser.add_argument('-sc', '--segm_conf', help='Путь к .CONF конфигурационному файлу сегментации')
#     parser.add_argument('-sp', '--segm_post', help='Путь к .VEC файлу апостериорных вероятностей сегментации')
#     parser.add_argument('-p', '--processes', default=None, type=int, help='Количество процессов для обработки файлов')
#     parser.add_argument('-l', '--log', dest='log', action='store_true', help='Логировать результат распознавания')
#     parser.add_argument('-dw', '--delete_wav', dest='delete_wav', action='store_true',
#                         help='Удалять .WAV файлы после распознавания')
#     parser.add_argument('-t', '--time', default=None, type=int,
#                         help='Пауза перед очередным сканированием директории в секундах')
#     parser.add_argument('-d', '--delta', default=None, type=int, help='Дельта, выдерживаемая до чтения файла в минутах')
# 
#     args = parser.parse_args()
# 
#     WAV_DIR = Path(args.wav)
#     OUTPUT_DIR = Path(args.output)
#     REC_MODEL = args.rec_model or 'model/final.mdl'
#     REC_GRAPH = args.rec_graph or 'model/HCLG.fst'
#     REC_WORDS = args.rec_words or 'model/words.txt'
#     REC_CONF = args.rec_conf or 'model/conf/mfcc.conf'
#     REC_ICONF = args.rec_iconf or 'model/conf/ivector_extractor.conf'
#     SEGM_MODEL = args.segm_model or 'model/final.raw'
#     SEGM_CONF = args.segm_conf or 'model/conf/mfcc_hires.conf'
#     SEGM_POST = args.segm_post or 'model/conf/post_output.vec'
#     PROCESSES = args.processes or cpu_count()
#     IS_LOG = args.log
#     IS_DELETE_WAV = args.delete_wav
#     SLEEP_TIME = args.time
#     DELTA_TIME = args.delta
# 
#     prep = data_preparator.DataPreparator(args.wav, str(OUTPUT_DIR), args.log)
#     LOG_DIR, TEMP_DIR, ASS_DIR, ERROR_DIR = prep.create_directories()
# 
#     while True:
#         wavs = glob.glob(str(WAV_DIR / '*.wav'))
#         if DELTA_TIME:
#             wavs = [wav for wav in wavs if time.time() - os.path.getmtime(wav) > DELTA_TIME * 60]
# 
#         if wavs:
#             print("Обнаружено {} .WAV файлов".format(len(wavs)))
#             if IS_LOG:
#                 try:
#                     log_name = str(LOG_DIR / str(time.strftime('%Y%m%d-%H%M%S') + '.log'))
#                     LOGGER = create_logger('logger', 'file', logging.DEBUG, log_name)
#                 except:
#                     raise Exception("Не удалось создать лог-файл")
#             else:
#                 LOGGER = create_logger('logger', 'stream', logging.INFO)
# 
#             try:
#                 CSV = str(OUTPUT_DIR / str('transcriptions_' + time.strftime('%Y%m%d-%H%M%S') + '.csv'))
#                 with open(CSV, 'w') as f:
#                     writer = csv.writer(f)
#                     writer.writerow(['Audio File', 'Start', 'End', 'Name', 'Text'])
#             except:
#                 raise Exception("Не удалось создать результирующий .CSV-файл")
# 
#             wavs = prep.rename_wav(wavs)
#             pool = Pool(PROCESSES)
#             LOGGER.info("Запуск распознавания речи")
#             LOGGER.debug("Количество процессов: {}".format(PROCESSES))
# 
#             for _ in tqdm(pool.imap(start_pipeline, wavs), total=len(wavs)):
#                 pass
#             pool.close()
#             pool.join()
#             LOGGER.info("Завершение распознавания речи")
# 
#         if SLEEP_TIME:
#             print("Мониторинг директории с .WAV файлами...")
#             time.sleep(SLEEP_TIME)
#         else:
#             break
# 
