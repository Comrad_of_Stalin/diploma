import pickle

from seq2seq.lang import SOS_token, EOS_token
from seq2seq.conf import device, MAX_LENGTH, hidden_size
from seq2seq.lang import tensorsFromPair, tensorFromSentence, Lang
from seq2seq.encoder import EncoderRNN, W2VEncoderRNN
from seq2seq.decoder import AttnDecoderRNN
import time
import os
import math
import random
import shutil
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.ticker as ticker
import numpy as np


def showPlot(points):
    plt.figure()
    fig, ax = plt.subplots()
    # this locator puts ticks at regular intervals
    loc = ticker.MultipleLocator(base=0.2)
    ax.yaxis.set_major_locator(loc)
    plt.plot(points)


def asMinutes(s):
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)


def timeSince(since, percent):
    now = time.time()
    s = now - since
    es = s / (percent)
    rs = es - s
    return '%s (- %s)' % (asMinutes(s), asMinutes(rs))


teacher_forcing_ratio = 0.5


def train(input_tensor, target_tensor, encoder, decoder, encoder_optimizer, decoder_optimizer, criterion, max_length=MAX_LENGTH):
    encoder_hidden = encoder.initHidden()

    encoder_optimizer.zero_grad()
    decoder_optimizer.zero_grad()

    input_length = input_tensor.size(0)
    target_length = target_tensor.size(0)

    encoder_outputs = torch.zeros(max_length, encoder.hidden_size, device=device)

    loss = 0

    for ei in range(input_length):
        encoder_output, encoder_hidden = encoder(
            input_tensor[ei], encoder_hidden)
        encoder_outputs[ei] = encoder_output[0, 0]

    decoder_input = torch.tensor([[SOS_token]], device=device)

    decoder_hidden = encoder_hidden

    use_teacher_forcing = True if random.random() < teacher_forcing_ratio else False

    if use_teacher_forcing:
        # Teacher forcing: Feed the target as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            loss += criterion(decoder_output, target_tensor[di])
            decoder_input = target_tensor[di]  # Teacher forcing

    else:
        # Without teacher forcing: use its own predictions as the next input
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            topv, topi = decoder_output.topk(1)
            decoder_input = topi.squeeze().detach()  # detach from history as input

            loss += criterion(decoder_output, target_tensor[di])
            if decoder_input.item() == EOS_token:
                break

    loss.backward()

    encoder_optimizer.step()
    decoder_optimizer.step()

    return loss.item() / target_length


def trainIters(encoder, decoder, optim, pairs, n_iters, print_every=1000, plot_every=100, learning_rate=0.01):
    start = time.time()
    plot_losses = []
    print_loss_total = 0  # Reset every print_every
    plot_loss_total = 0  # Reset every plot_every

    encoder_optimizer = optim.SGD(encoder.parameters(), lr=learning_rate)
    decoder_optimizer = optim.SGD(decoder.parameters(), lr=learning_rate)
    training_pairs = [tensorsFromPair(random.choice(pairs))
                      for i in range(n_iters)]
    criterion = nn.NLLLoss()

    for iter in range(1, n_iters + 1):
        training_pair = training_pairs[iter - 1]
        input_tensor = training_pair[0]
        target_tensor = training_pair[1]

        loss = train(input_tensor, target_tensor, encoder,
                     decoder, encoder_optimizer, decoder_optimizer, criterion)
        print_loss_total += loss
        plot_loss_total += loss

        if iter % print_every == 0:
            print_loss_avg = print_loss_total / print_every
            print_loss_total = 0
            print('%s (%d %d%%) %.4f' % (timeSince(start, iter / n_iters),
                                         iter, iter / n_iters * 100, print_loss_avg))

        if iter % plot_every == 0:
            plot_loss_avg = plot_loss_total / plot_every
            plot_losses.append(plot_loss_avg)
            plot_loss_total = 0

    showPlot(plot_losses)


def evaluate(encoder, decoder, input_lang, output_lang, sentence, max_length=MAX_LENGTH):
    with torch.no_grad():
        input_tensor = tensorFromSentence(input_lang, sentence, device)
        input_length = input_tensor.size()[0]
        encoder_hidden = encoder.initHidden()

        encoder_outputs = torch.zeros(max_length, encoder.hidden_size, device=device)

        for ei in range(input_length):
            encoder_output, encoder_hidden = encoder(input_tensor[ei],
                                                     encoder_hidden)
            encoder_outputs[ei] += encoder_output[0, 0]

        decoder_input = torch.tensor([[SOS_token]], device=device)  # SOS

        decoder_hidden = encoder_hidden

        decoded_words = []
        decoder_attentions = torch.zeros(max_length, max_length)

        for di in range(max_length):
            decoder_output, decoder_hidden, decoder_attention = decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            decoder_attentions[di] = decoder_attention.data
            topv, topi = decoder_output.data.topk(1)
            if topi.item() == EOS_token:
                decoded_words.append('<EOS>')
                break
            else:
                decoded_words.append(output_lang.index2word[topi.item()])

            decoder_input = topi.squeeze().detach()

        return decoded_words, decoder_attentions[:di + 1]


def evaluateRandomly(encoder, decoder, inp_lang, out_lang, pairs, n=10):
    for i in range(n):
        pair = random.choice(pairs)
        print('>', pair[0])
        print('=', pair[1])
        output_words, attentions = evaluate(encoder, decoder, inp_lang, out_lang, pair[0])
        output_sentence = ' '.join(output_words)
        print('<', output_sentence)
        print('')


def load_decoder(path, output_lang, device_type='cuda', hidden_size=hidden_size):
    model = AttnDecoderRNN(hidden_size=hidden_size, output_size=output_lang.n_words, max_length=MAX_LENGTH)
    model.load_state_dict(torch.load(path, map_location=device_type))
    model.eval()
    return model


def load_encoder(path, input_lang, device_type='cuda', hidden_size=hidden_size):
    model = W2VEncoderRNN(np.zeros((input_lang.n_words, 300), dtype=np.float32), hidden_size=hidden_size) # EncoderRNN(input_size=input_lang.n_words, hidden_size=hidden_size)
    model.load_state_dict(torch.load(path, map_location=device_type))
    model.eval()
    return model


def load_lang(path):
    name_path = os.path.join(path, 'name')
    w2i_path = os.path.join(path, 'word2index')
    w2c_path = os.path.join(path, 'word2count')
    iw_path = os.path.join(path, 'index2word')
    nw_path = os.path.join(path, 'n_words')
    with open(name_path, 'rb') as f:
        name = pickle.load(f)
    with open(w2i_path, 'rb') as f:
        word2index = pickle.load(f)
    with open(w2c_path, 'rb') as f:
        word2count = pickle.load(f)
    with open(iw_path, 'rb') as f:
        index2word = pickle.load(f)
    with open(nw_path, 'rb') as f:
        n_words = pickle.load(f)
    lang = Lang(name)
    lang.word2index = word2index
    lang.word2count = word2count
    lang.index2word = index2word
    lang.n_words = n_words
    return lang


def load_dump(path, device_type='cuda', hidden_size=hidden_size):
    enc_path = os.path.join(path, 'models/encoder')
    dec_path = os.path.join(path, 'models/decoder')
    inp_lang_path = os.path.join(path, 'langs/inp_lang')
    out_lang_path = os.path.join(path, 'langs/out_lang')
    inp_lang = load_lang(inp_lang_path)
    out_lang = load_lang(out_lang_path)
    encoder = load_encoder(enc_path, inp_lang, device_type=device_type, hidden_size=hidden_size)
    decoder = load_decoder(dec_path, out_lang, device_type=device_type, hidden_size=hidden_size)
    return encoder, decoder, inp_lang, out_lang


def save_model(model, path):
    torch.save(model.state_dict(), path)


def save_lang(lang, path):
    name_path = os.path.join(path, 'name')
    w2i_path = os.path.join(path, 'word2index')
    w2c_path = os.path.join(path, 'word2count')
    iw_path = os.path.join(path, 'index2word')
    nw_path = os.path.join(path, 'n_words')
    with open(name_path, 'wb') as f:
        pickle.dump(lang.name, f)
    with open(w2i_path, 'wb') as f:
        pickle.dump(lang.word2index, f)
    with open(w2c_path, 'wb') as f:
        pickle.dump(lang.word2count, f)
    with open(iw_path, 'wb') as f:
        pickle.dump(lang.index2word, f)
    with open(nw_path, 'wb') as f:
        pickle.dump(lang.n_words, f)


def save_dump(encoder, decoder, inp_lang, out_lang, path):
    if os.path.exists(path):
        print('[WARN] path ' + os.path.abspath(path) + ' exists. Replace it with new dump.')
        shutil.rmtree(path)
    os.makedirs(path)
    os.makedirs(os.path.join(path, 'models'))
    os.makedirs(os.path.join(path, 'langs'))
    os.makedirs(os.path.join(path, 'langs/inp_lang'))
    os.makedirs(os.path.join(path, 'langs/out_lang'))
    enc_path = os.path.join(path, 'models/encoder')
    dec_path = os.path.join(path, 'models/decoder')
    inp_lang_path = os.path.join(path, 'langs/inp_lang')
    out_lang_path = os.path.join(path, 'langs/out_lang')
    save_model(encoder, enc_path)
    save_model(decoder, dec_path)
    save_lang(inp_lang, inp_lang_path)
    save_lang(out_lang, out_lang_path)
    print('[Saved]')
