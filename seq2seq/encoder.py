import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from seq2seq.conf import device


class Word2VecEmbedding(nn.Module):
    def __init__(self, weight_matrix):
        super(Word2VecEmbedding, self).__init__()
        n_tokens, e_dim = weight_matrix.shape
        self.emb = nn.Embedding(n_tokens, e_dim)
        self.emb.load_state_dict({'weight': torch.as_tensor(weight_matrix).type(torch.FloatTensor)})

    def forward(self, x):
        return self.emb(x)


class W2VEncoderRNN(nn.Module):
    def __init__(self, weight_matrix, hidden_size=256):
        super(W2VEncoderRNN, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = Word2VecEmbedding(weight_matrix=weight_matrix)
        self.gru = nn.GRU(300, hidden_size)

    def forward(self, input, hidden):
        embedded = self.embedding(input).view(1, 1, -1)
        output = embedded
        output, hidden = self.gru(output, hidden)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=device)


class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size)

    def forward(self, input, hidden):
        embedded = self.embedding(input).view(1, 1, -1)
        output = embedded
        output, hidden = self.gru(output, hidden)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size, device=device)
