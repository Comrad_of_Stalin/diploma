import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

MAX_LENGTH = 10

hidden_size = 256

eng_prefixes = (
    "i am ", "i m ",
    "he is", "he s ",
    "she is", "she s ",
    "you are", "you re ",
    "we are", "we re ",
    "they are", "they re "
)
