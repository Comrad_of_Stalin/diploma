from os.path import join as pj


class RequestType:
    WAV_FILE = 0
    TEXT_FILE = 1
    TEXT_LINE = 2

    @classmethod
    def parse_request(cls, request_str):
        sep = ':'
        splitted = request_str.split(sep)
        assert len(splitted) == 2
        input_type, output_type = splitted

        spec = {
            'input_type': input_type,
            'output_type': output_type
        }

        return spec


class Request:
    def __init__(self, body, dst_path, format_str):
        self.body = body
        self.dst_path = dst_path
        self.spec = RequestType.parse_request(format_str)


class RequestCollection:

    def __init__(self, app, request, req_id):
        text = request.form.get('text') or None
        wav_path = request.files.get('wav-attachment-file')
        text_path = request.files.get('text-attachment-file')

        format_str = request.form.get('format')

        self.requests = []

        if text is not None:
            dst_path = pj(app.config['REQUEST_DIR'], 'text_raw_out.{}'.format(self._parse_ext(format_str)))
            self.requests.append(Request(text, dst_path, 'text2{}'.format(format_str)))
        if text_path is not None:
            with open(text_path) as f:
                text_str = f.read().strip().lower()
            dst_path = pj(app.config['REQUEST_DIR'], 'text_file_out.{}'.format(self._parse_ext(format_str)))
            self.requests.append(Request(text_str, dst_path, 'text2{}'.format(format_str)))
        if wav_path is not None:
            dst_path = pj(app.config['REQUEST_DIR'], 'wav_file_out.{}'.format(self._parse_ext(format_str)))
            self.requests.append(Request(text_str, dst_path, 'wav2{}'.format(format_str)))

    def _parse_ext(self, format_str):
        if format_str == 'wav':
            return 'wav'
        if format_str == 'text':
            return 'txt'
